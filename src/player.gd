extends KinematicBody2D

const GRAVITY: float = 1000.0
const DEFAULT_SPEED: float = 500.0

const DEFAULT_JUMP_SPEED: float = 2200.0

var direction: Vector2 = Vector2(0,0)
var move_speed: float = DEFAULT_SPEED
var jump_speed: float = 0.0
var is_on_ladder: bool = false
var has_double_jump: bool = true

func compute_direction():
	var x_dir = 0
	
	if Input.is_action_pressed("player_right") and not(Input.is_action_pressed("player_left")):
		x_dir = 1
	elif Input.is_action_pressed("player_left") and not(Input.is_action_pressed("player_right")):
		x_dir = -1
	
	var y_dir = GRAVITY - jump_speed
	
	if is_on_ladder:
		y_dir = min(y_dir, 0)
		if Input.is_action_pressed("player_up") and not(Input.is_action_pressed("player_down")):
			y_dir -= move_speed
		elif Input.is_action_pressed("player_down") and not(Input.is_action_pressed("player_up")):
			y_dir += move_speed
		
	direction = Vector2(x_dir, y_dir)



func compute_jump():
	if (Input.is_action_just_pressed("player_jump")
	):
		if (	jump_speed <= 0  and (
			is_on_floor() or 
			is_on_ladder)
		):
			jump_speed = DEFAULT_JUMP_SPEED
			$jump_sound.play()
		elif has_double_jump:
			has_double_jump = false
			$jump_sound.play()
			jump_speed = DEFAULT_JUMP_SPEED
	else:
		if jump_speed > 0:
			jump_speed -= GRAVITY / 10
		else:
			jump_speed = 0
		if is_on_floor() or is_on_wall():
			has_double_jump = true

func compute_animation():
	if direction.x != 0:
		$AnimatedSprite.play("walk_right")
		if direction.x < 0:
			$AnimatedSprite.flip_h = true
		else:
			$AnimatedSprite.flip_h = false
	else:
		$AnimatedSprite.play("pause_right")

func _physics_process(delta):
	compute_direction()
	compute_jump()
	compute_animation()
	move_and_slide(Vector2(direction.x * move_speed, direction.y), Vector2(0, -1))

func _ready():
	pass # Replace with function body.

func _on_ladder_body_entered(body):
	is_on_ladder = true


func _on_ladder_body_exited(body):
	is_on_ladder = false
