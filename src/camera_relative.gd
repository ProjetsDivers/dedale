extends Camera2D

export(NodePath) var relative_item

func _process(delta):
	global_position = get_node(relative_item).global_position + Vector2(0, -100)

func _ready():
	pass # Replace with function body.