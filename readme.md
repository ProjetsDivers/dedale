# Dedale

Par Cédric Charière Fiedler, 2018

## Prérequis

- Godot 3.1 (Alpha uniquement)

## A propos

Dédale est un jeu de plateforme en cours de réalisation.
Vous pouvez l'utiliser en tant que blueprint sans aucun soucis ;)

![Capture d'écran du jeu](./screenshots/screen-01-2019-10-31.jpg)

## Participer

Vous êtes invités à proposer des évolutions / ressources, le projet est open-source !
## Fonctionnalités

- Tilemap avec Collision
- Lumière 2D
- Plateformer basique :
    - Saut
    - Echelle
    - Double saut

## Feuille de route

- Son
    - Ajout des sons lors du déplacement
    - Ajout des sons lors de l'atterrissage au sol
    - Son lors d'un double saut
- Ambiance
    - Bruits d'engrenages
    - Bruits de cave (canalisations, crissements métalliques)
- Sprite Joueur
    - Animation repos
    - Animation Saut
    - Animation déplacement